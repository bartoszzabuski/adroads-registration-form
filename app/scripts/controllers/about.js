'use strict';

/**
 * @ngdoc function
 * @name adroadsRegistrationFormApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the adroadsRegistrationFormApp
 */
angular.module('adroadsRegistrationFormApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
