'use strict';

/**
 * @ngdoc function
 * @name adroadsRegistrationFormApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adroadsRegistrationFormApp
 */
angular.module('adroadsRegistrationFormApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
